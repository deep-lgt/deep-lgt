import numpy as np
from action.mcrg import map_vec, action_param, Stot
from flow import integrator
import tensorflow as tf

nx = 16
ny = 16

def make_lattice(x):
    a = x[:,::2]
    b = x[:,1::2]

    phi = 2*np.pi*a
    ct = 2*b-1
    st = np.sqrt(1-ct*ct)

    lat = np.array([st*np.cos(phi),
                    st*np.sin(phi),
                    ct])

    return np.reshape(lat,[x.shape[0],3,nx,ny])

def get_actions(x):
    lats = make_lattice(x)
    actions = []
    for lat in lats:
        actions.append(Stot(lat,action_param))
    return np.array(actions)

def func(x):
    return tf.stop_gradient(tf.py_function(get_actions,[x],tf.float32))

integrate = integrator.Integrator(func, 2*nx*ny, mode='linear',blob=True,layers=8,unet=True)
integrate.make_optimizer(nsamples=500,learning_rate=1e-3)

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    integrate.optimize(sess, epochs=1000, printout=1)
