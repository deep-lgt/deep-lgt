#define FP double
#include <stdio.h>
#include <cuda.h>
#include <stdlib.h>
#include <math.h>

__global__ void gpu_matrixmult(FP *A,FP *B, FP *C, int N, int P, int M) {
    int col = threadIdx.x + blockDim.x * blockIdx.x;
    int row = threadIdx.y + blockDim.y * blockIdx.y;

    int indexB = col;
    int index = row*M + col;
  
    if(col < M && row < N) {
        C[index] = 0.;
        //go along row of A, down column of B 
        int indexA;
        for (indexA = row*P; indexA < (row*P + P); indexA++, indexB+=M);
            C[index] += A[indexA]*B[indexB];
    }
}


void cpu_matrixmult(FP *A,FP *B, FP *C, int N, int P, int M) {
    for(int k=0; k<P; k++){
        for(int i=0; i<N; i++){
            FP r = A[i*P+k];
            for(int j=0; j<M; j++){
                C[i*M+j] -= r*B[k*M+j];
            }
        }
    }
}

int main(int argc, char *argv[]) {
    int i, j, index;
    
    int gpucount = 0; // Count of available GPUs
    int gpunum = 0; // Device number to use

    int Grid_Dimx; //Grid dimension, x and y, square
    int Grid_Dimy; //Grid dimension, x and y, square
    int Block_Dimx; //Block dimension, x and y, square
    int Block_Dimy; //Block dimension, x and y, square
    int N; // matrix dimension
    int P;
    int M;

    FP *A,*B,*C;
    FP *dev_A, *dev_B, *dev_C;

    cudaEvent_t start, stop; // using cuda events to measure time
    float elapsed_time_ms; // which is applicable for asynchronous code also
    cudaError_t errorcode;
    


    // --------------------SET PARAMETERS AND DATA -----------------------

    errorcode = cudaGetDeviceCount(&gpucount);
    if (errorcode == cudaErrorNoDevice) {
        printf("No GPUs are visible\n");
        exit(-1);
    }
    else printf("Device count = %d\n",gpucount);

    if (argc!=8) {
        printf("Usage: matmul <N> <P> <M>  <block dim x> <block dom y> <grid dim x> <grid dim y>\n");
        exit (-1);
    }


    N = atoi(argv[1]);
    P = atoi(argv[2]);
    M = atoi(argv[3]);
    Block_Dimx = atoi(argv[4]);
    Block_Dimy = atoi(argv[5]);
    Grid_Dimx = atoi(argv[6]);
    Grid_Dimy = atoi(argv[7]);


    if (Block_Dimx*Block_Dimy > 1024) {
        printf("Error, too many threads in block\n");
        exit (-1);
    }
    //what does block dim matter? 
    if (Grid_Dimx*Block_Dimx < M) { //x goes through columns, goes to M 
        printf("Error, number of threads in x dimension less than number of array elements\n");
        exit (-1);
    }
    if (Grid_Dimy*Block_Dimy < N) { //y goes through rows, goes to N 
        printf("Error, number of threads in y dimension less than number of array elements\n");
        exit(-1);
    }

    cudaSetDevice(gpunum);
    printf("Using device %d\n",gpunum);
  
    printf("Matrix Dimension (%d,%d) x (%d,%d) -> (%d,%d)\n",N, P, P, M, N, M);
    printf("Block_Dim = (%d,%d),  Grid_Dim = (%d,%d)\n",Block_Dimx, Block_Dimy, Grid_Dimx, Grid_Dimy);

    dim3 Grid(Grid_Dimx, Grid_Dimy); //Grid structure
    dim3 Block(Block_Dimx, Block_Dimy); //Block structure

    int sizeA = N*P;
    int sizeB = P*M;
    int sizeC = N*M;
    A = (FP*) calloc(sizeA, sizeof(FP)); // dynamically allocated memory for arrays on host
    B = (FP*) calloc(sizeB, sizeof(FP));
    C = (FP*) calloc(sizeC, sizeof(FP)); // results from GPU
       
    srand(12345);
    for(i=0; i<N; i++)
        for(j=0; j<P; j++) {
            index = i*P+j;
            A[index] = (FP) rand() / (FP) RAND_MAX;
    }
    for(int i=0;i < P;i++)
        for(int j=0;j < M;j++) {
            int index = i*M+j;
            B[index] = (FP) rand() / (FP) RAND_MAX;
            //      b[i * n + j] = (FP) i+j; // may be helpful for debugging
    }
    
    // ------------- COMPUTATION DONE ON GPU ----------------------------

    cudaMalloc((void**)&dev_A, sizeA*sizeof(FP)); // allocate memory on device
    cudaMalloc((void**)&dev_B, sizeB*sizeof(FP));
    cudaMalloc((void**)&dev_C, sizeC*sizeof(FP));

    cudaMemcpy(dev_A, A , sizeA*sizeof(FP) ,cudaMemcpyHostToDevice);
    cudaMemcpy(dev_B, B , sizeB*sizeof(FP) ,cudaMemcpyHostToDevice);

    cudaEventCreate(&start); // instrument code to measure start time
    cudaEventCreate(&stop);
  
    cudaEventRecord(start, 0);
    // cudaEventSynchronize(start); // not needed

    gpu_matrixmult<<<Grid,Block>>>(dev_A,dev_B,dev_C,N,P,M);

    cudaEventRecord(stop, 0); // instrument code to measure end time
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&elapsed_time_ms, start, stop );

    cudaMemcpy(C,dev_C, sizeC*sizeof(FP), cudaMemcpyDeviceToHost);

    printf("Time to calculate results on GPU: %f ms.\n", elapsed_time_ms); // exec. time

    FILE *fp;
    char filename[64];
    sprintf(filename, "results1a/rectangular_DOUBLE_N%d_P%d_M%d_bdx%d_bdy%d_gdx%d_gdy%d.txt", N, P, M, Block_Dimx, Block_Dimy, Grid_Dimx, Grid_Dimy);
    fp = fopen(filename, "w+");
 
    fprintf(fp, "Time to calculate results on GPU: %f ms.\n", elapsed_time_ms); // exec. time

    fclose(fp);


    /*
    // ------------- COMPUTATION DONE ON HOST CPU ----------------------------
    // DEBUGGING USE ONLY (AND FOR LIMITED NUMBERS OF TIMING RUNS)

    cudaEventRecord(start, 0); // use same timing
    // cudaEventSynchronize(start); // not needed


    cpu_matrixmult(A,B,C, N, P, M); // do calculation on host (NOTE: This computes the diff with GPU result.)

    cudaEventRecord(stop, 0); // instrument code to measue end time
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&elapsed_time_ms, start, stop );

    printf("Time to calculate results on CPU: %f ms.\n", elapsed_time_ms); // exec. time
    */
    /*
    // ------------------- check device creates correct results -----------------
    double error, suma, sumb, sumc, ai, bi, ci;
    suma = 0.; sumb = 0; sumc = 0;
    for(i=0; i< N*P; i++) {
        ai = (double) A[i];
        suma += ai*ai;
    }   
    for(i=0; i< P*M; i++){
        bi = (double) B[i];
        sumb += bi*bi;
    }
    for(i=0; i< N*M; i++){
        ci = (double) C[i];
        sumc += ci*ci;
    }
    suma = sqrt(suma);
    sumb = sqrt(sumb);
    sumc = sqrt(sumc);
    error =  sumc/(sqrt(N*M)*suma*sumb);
    printf("Scaled error between GPU and CPU: %e\n", error);

    double Fnorm = 0;
    for (i=0; i<N*M; i++) Fnorm += C[i]*C[i];
    printf("Forbenius norm of C between CPU and GPU: %f\n", Fnorm);
    */
    
    // -------------- clean up ---------------------------------------
    free(A);
    free(B);
    free(C);
    cudaFree(dev_A);
    cudaFree(dev_B);
    cudaFree(dev_C);
    cudaEventDestroy(start);
    cudaEventDestroy(stop);
    return 0;
}
