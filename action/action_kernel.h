#ifndef spins_action
#define spins_action


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cuda.h>
#include <stdbool.h>
#include <time.h>
#define PI 3.14159265
#define nx 16
#define ny 16
#define d_min 0
#define d_max 10
#define num_terms 2
#define iterations 10000
#define num_demon_systems 100 // should be about autocorelation time 




__global__ void gpu_action(double* lattice, double* action_param_beta, int* action_param_n, double* S_lattice, double* S_tot){

    /*
    global function will take a lattice configuration and return the total action 
    calls device functions:     S_site
                                total_action                     
    */

    // the thread index will determine spin that will compute action
    // due to its neighbors 
    int i = threadIdx.x + blockDim.x * blockIdx.x;
    int j = threadIdx.y + blockDim.y * blockIdx.y;
    
    index_ij = i*ny + j;
    
    //threads with index ij hold S_lattice[index_ij]
    S_lattice[index_ij] = 0.0;

    // if this thread index corresponds to an actual lattice point
    if(i < nx && j < ny){
        
        //compute the action at the point (i,j)
        //define a buffer to hold the values of action terms 
        double dSij[num_terms];

        // call device function to get the action term values at this site
        S_site(lattice, action_param_n, i, j, dSij);

        //sum up each site with factor of 1/2 for double counting, sum action beta_a S_a 
        for(int a = 0; a < num_terms; a++) S_lattice[index_ij] += 0.5*dSij[a]*action_param_beta[a];
        
        // call device function to add up the elements of S_lattice 
        //S_tot[0] = REDUCE(S_lattice) // blockDim.x maybe * blockDim.y = BLOCKSIZE 
    }
}




__device__ int index_lattice(int i, int j, int k){
    // row major 
    return i*ny*3 + j*3 + k;
}


__device__ void dotProduct(double* dot, double* vect_A, double* vect_B, int n) { 
    dot[0] = 0.0; 
    for (int i = 0; i < n; i++) 
        dot[0] = dot[0] + vect_A[i] * vect_B[i];  
}


__device__ void S_site(double* lattice, int* action_param_n, int site_i, int site_j, double* dSij){
    /*
    Calculate the lattice action associated with site (xi, xj).
    assume periodic boundary conditions - note that all terms 
    have a factor of 1/2 by their definition in Hasenbusch paper
    */
    
    // get local spin value
    double local_spin[3];
    for(int k = 0; k < 3; k++) local_spin[k] = lattice[index_lattice(site_i, site_j, k)];
    
    // Loop over interaction terms
    for(int a=0; a < num_terms; a++){
        int n = action_param_n[a];

        double neighbor1[3];
        double neighbor2[3];
        double neighbor3[3];
        double neighbor4[3];
        double neighbor5[3];
        double neighbor6[3];
        double neighbor7[3];
        double neighbor8[3];
        
        
        if(a == 0 || a == 5 || a == 10){ // v= (1,0)
            for(int k = 0; k < 3; k++){
                neighbor1[k] = lattice[index_lattice((site_i + 1 + nx)%nx, site_j, k)];
                neighbor2[k] = lattice[index_lattice((site_i - 1 + nx)%nx, site_j, k)];
                neighbor3[k] = lattice[index_lattice(site_i, (site_j + 1 + ny)%ny, k)];
                neighbor4[k] = lattice[index_lattice(site_i, (site_j - 1 + ny)%ny, k)];
            }
            dSij[a] =0.5*(pow(dotProduct(local_spin, neighbor1, 3), n) 
                        + pow(dotProduct(local_spin, neighbor2, 3), n)
                        + pow(dotProduct(local_spin, neighbor3, 3), n)
                        + pow(dotProduct(local_spin, neighbor4, 3), n));
        }
          
        if(a == 1 || a == 6 || a == 11){ // v= (1,1)
            for(int k = 0; k < 3; k++){
                neighbor1[k] = lattice[index_lattice((site_i + 1 + nx)%nx, (site_j + 1 + ny)%ny, k)];
                neighbor2[k] = lattice[index_lattice((site_i - 1 + nx)%nx, (site_j + 1 + ny)%ny, k)];
                neighbor3[k] = lattice[index_lattice((site_i + 1 + nx)%nx, (site_j - 1 + ny)%ny, k)];
                neighbor4[k] = lattice[index_lattice((site_i - 1 + nx)%nx, (site_j - 1 + ny)%ny, k)];
            }
            dSij[a] =0.5*(pow(dotProduct(local_spin, neighbor1, 3), n)
                        + pow(dotProduct(local_spin, neighbor2, 3), n)
                        + pow(dotProduct(local_spin, neighbor3, 3), n)
                        + pow(dotProduct(local_spin, neighbor4, 3), n));
        }
    
        if(a == 2 || a == 7){ // v= (2,0)
            for(int k = 0; k < 3; k++){
                neighbor1[k] = lattice[index_lattice((site_i + 2 + nx)%nx, site_j, k)];
                neighbor2[k] = lattice[index_lattice((site_i - 2 + nx)%nx, site_j, k)];
                neighbor3[k] = lattice[index_lattice(site_i, (site_j + 2 + ny)%ny, k)];
                neighbor4[k] = lattice[index_lattice(site_i, (site_j - 2 + ny)%ny, k)];
            }
            dSij[a] =0.5*(pow(dotProduct(local_spin, neighbor1, 3), n)
                        + pow(dotProduct(local_spin, neighbor2, 3), n)
                        + pow(dotProduct(local_spin, neighbor3, 3), n)
                        + pow(dotProduct(local_spin, neighbor4, 3), n));
        
        }
    
        if(a == 3 || a == 8){ // v= (2,1)
            for(int k = 0; k < 3; k++){
                neighbor1[k] = lattice[index_lattice((site_i + 2 + nx)%nx, (site_j + 1 + ny)%ny, k)];
                neighbor2[k] = lattice[index_lattice((site_i + 2 + nx)%nx, (site_j - 1 + ny)%ny, k)];
                neighbor3[k] = lattice[index_lattice((site_i - 2 + nx)%nx, (site_j + 1 + ny)%ny, k)];
                neighbor4[k] = lattice[index_lattice((site_i - 2 + nx)%nx, (site_j - 1 + ny)%ny, k)];
                neighbor5[k] = lattice[index_lattice((site_i + 1 + nx)%nx, (site_j + 2 + ny)%ny, k)];
                neighbor6[k] = lattice[index_lattice((site_i + 1 + nx)%nx, (site_j - 2 + ny)%ny, k)];
                neighbor7[k] = lattice[index_lattice((site_i - 1 + nx)%nx, (site_j + 2 + ny)%ny, k)];
                neighbor8[k] = lattice[index_lattice((site_i - 1 + nx)%nx, (site_j - 2 + ny)%ny, k)];
            }
            dSij[a] =0.5*(pow(dotProduct(local_spin, neighbor1, 3), n)
                        + pow(dotProduct(local_spin, neighbor2, 3), n)
                        + pow(dotProduct(local_spin, neighbor3, 3), n)
                        + pow(dotProduct(local_spin, neighbor4, 3), n)
                        + pow(dotProduct(local_spin, neighbor5, 3), n)
                        + pow(dotProduct(local_spin, neighbor6, 3), n)
                        + pow(dotProduct(local_spin, neighbor7, 3), n)
                        + pow(dotProduct(local_spin, neighbor8, 3), n));
        }

        if(a == 4 || a == 9){ // v= (3,0)
            for(int k = 0; k < 3; k++){
                neighbor1[k] = lattice[index_lattice((site_i + 3 + nx)%nx, site_j, k)];
                neighbor2[k] = lattice[index_lattice((site_i - 3 + nx)%nx, site_j, k)];
                neighbor3[k] = lattice[index_lattice(site_i, (site_j + 3 + ny)%ny, k)];
                neighbor4[k] = lattice[index_lattice(site_i, (site_j - 3 + ny)%ny, k)];
            }
            dSij[a] =0.5*(pow(dotProduct(local_spin, neighbor1, 3), n)
                        + pow(dotProduct(local_spin, neighbor2, 3), n)
                        + pow(dotProduct(local_spin, neighbor3, 3), n)
                        + pow(dotProduct(local_spin, neighbor4, 3), n));
        }
    }

    return;
}



#endif
