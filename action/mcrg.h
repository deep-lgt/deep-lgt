#ifndef MCRG_H
#define MCRG_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <time.h>
#include "spins_action.h"
#define PI 3.14159265
#define nx 16
#define ny 16
#define d_min 0
#define d_max 10
#define num_terms 2
#define iterations 1000000
#define num_demon_systems 100 // should be about autocorelation time 

void map_vec(double a, double b, double* vector);
void crossProduct(double* vect_A, double* vect_B, double* vector);
void axis_rot(double* v, double* w, double* vector);
void epsilon_update(double* v, double epsilon, double* new_v);
void random_lattice(double* lattice);
void metropolis_sweep(double* lattice, double* deltaS, double* avg_metrop_accept_rate, double* action_param_beta, int* action_param_n, int n_hits, double epsilon, int iter, double num_metrop_steps_inv);
void microcanonical_demon_update(double* avg_accept, double num_demon_steps_inv,  double* last_steps, double* lattice, double* action_param_beta, int* action_param_n, double epsilon, int file_name_index, int system_cycle);
void measure_corr(double* lattice, int measurement_num);

#endif
