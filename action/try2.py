import numpy as np
import matplotlib.pyplot as plt
import os

"""
for i in range(10):
    os.system("rm *%i.txt"%i)
"""


dS = []
for i in range(20000):
    dat = np.loadtxt("dS/deltaS_N20k_%i.txt"%i)
    dS.extend(dat)

plt.hist(dS)
plt.show()

expdS = [np.exp(-dSi) for dSi in dS]

print(np.mean(expdS))

