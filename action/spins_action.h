#ifndef SPINS_ACTION_H 
#define SPINS_ACTION_H


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <time.h>
#define PI 3.14159265
#define nx 16
#define ny 16
#define d_min 0
#define d_max 10
#define num_terms 2
#define iterations 1000000
#define num_demon_systems 100 // should be about autocorelation time 

int index_lattice(int i, int j, int k);
double dotProduct(double* vect_A, double* vect_B, int n);
void S_site(double* lattice, int* action_param_n, int site_i, int site_j, double* dSij);
void S_tot(double* lattice, double* action_param_beta, int* action_param_n, double* S);

#endif
