%module pymcrg

%{
#define SWIG_FILE_WITH_INIT
%}


%{
    #include "spins_action.h"
    #include "mcrg.h"
%}

%include "carrays.i"

%array_class(double, double_array);

%{

void print_array(double *x, int size) {
    for(int i = 0; i < size; ++i) {
        printf("%f ", x[i]);
    }

    printf("\n");
   
    return;
}

%}

void print_array(double *x, int size);

%include "spins_action.h"
%include "mcrg.h"
