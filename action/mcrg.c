#include "mcrg.h"

void map_vec(double a, double b, double* vector){

    /*
    Map two numbers (or two 1d numpy array of same lengths), a and b, in [0, 1)
    to a vector [x, y, z] with unit norm.
    If a and b are randomly distributed in [0, 1) interval, the function will
    return randomly distributed vectors on the sphere.
    */
    double phi = 2*PI*a;
    double theta = acos(2*b-1);
    vector[0] = sin(theta)*cos(phi);
    vector[1] = sin(theta)*sin(phi);
    vector[2] = cos(theta);

    return;
}



void crossProduct(double* vect_A, double* vect_B, double* vector) {
    vector[0] = vect_A[1] * vect_B[2] - vect_A[2] * vect_B[1]; 
    vector[1] = vect_A[2] * vect_B[0] - vect_A[0] * vect_B[2]; 
    vector[2] = vect_A[0] * vect_B[1] - vect_A[1] * vect_B[0]; 
    return;
} 



void axis_rot(double* v, double* w, double* vector){
    /*
    Given a fixed unit vector v (one of our spins) in cartesian coordinates [x,y,z],
    rotate vector w such that if w == [0, 0, 1], the function will output
    vector v. This is done by using Rodrigues' rotation formula.
   
    We will use a perturbation of w = [0,0,1] to get a perturbation of v 
    */
    // Normalize v
    double norm_v = sqrt(dotProduct(v,v,3));
    double v_norm[3] = {v[0]/norm_v, v[1]/norm_v, v[2]/norm_v};
    // Find the axis of rotation, k = [0,0,1] x v_norm
    double z[3] = {0,0,1};
    double k[3];
    crossProduct(z,v_norm, k);
    double norm_k =  sqrt(dotProduct(k,k,3));
    double k_norm[3] = {k[0]/norm_k, k[1]/norm_k, k[2]/norm_k};
    // Find angle of rotation, phi cosphi = v dot [0,0,1]
    double cosphi = dotProduct(v_norm,z,3);
    double phi = acos(cosphi);
    // Apply rotation formula
    double kxw[3]; //k cross w
    crossProduct(k_norm,w,kxw);
    for(int i=0; i<3; i++) vector[i] = w[i]*cosphi + kxw[i]*sin(phi) + k_norm[i]*dotProduct(k_norm, w,3)*(1-cosphi);
    
    return;   
}



void epsilon_update(double* v, double epsilon, double* new_v){
    /*
    Given a vector with unit norm, return a new vector, w, such that w is chosen
    randomly around the cone centered at vector v.
    epsilon is in the interval [0, 1) and the angle of the cone is given
    by arccos(2*(1-epsilon) - 1).
    */
    // First find the new random vector around a cone centered at vector [0, 0, 1]
    double a = (double)rand() / (double)RAND_MAX; 
    double b = (double)rand() / (double)RAND_MAX; 
    double c = 1.-b*epsilon;
    double dv[3];
    map_vec(a,c,dv); //from a and c get the vector dv
    //rotate this vector to be a perturbation of v 
    axis_rot(v,dv,new_v);
    return;
}




void random_lattice(double* lattice){
    /*
    Return a random lattice of spins (one dimensional indexing)
    lattice[i*ny*3 + j*3 + k] gives ith row (0 to nx -1) jth column (0 to ny -1)
    kth component of spin (0,1,2)
    */
    //loop over sites 
    for(int i = 0; i < nx; i++){
        for(int j = 0; j < nx; j++){
            double a = (double)rand() / (double)RAND_MAX; 
            double b = (double)rand() / (double)RAND_MAX; 
            // get a random spin vector at this site
            double spin_vector[3];
            map_vec(a,b,spin_vector);
            // define lattice spin at this site 
            for(int k=0; k<3; k++) lattice[index_lattice(i,j,k)] = spin_vector[k]; 
        }
    }

    return;
}



void metropolis_sweep(double* lattice, double* deltaS, double* avg_metrop_accept_rate, double* action_param_beta, int* action_param_n, int n_hits, double epsilon, int iter, double num_metrop_steps_inv){
    /*
    Perform a sweep of metropolis update to the input lattice. Each site will be
    updated n times regardless of being accepted or rejected.
    epsilon is in [0, 1) that controls how big the proposed updates will be with
    0 being identity (no changes in updates).
    */
    for(int i = 0; i < nx; i++){
        for(int j = 0; j < ny; j++){
            //multi-hit updates
            for(int n = 0; n < n_hits; n++){
                double dSij_old[num_terms];
                double dSij_new[num_terms];
                double old_spin[3];
                double new_spin[3];
          
                // get initial spin state (in case need to flip back)
                for(int k = 0; k < 3; k++) old_spin[k] = lattice[index_lattice(i,j,k)];
                //get initial action
                S_site(lattice, action_param_n, i, j, dSij_old);

                // get new spin state
                epsilon_update(old_spin, epsilon, new_spin);
                // update lattice site 
                for(int k = 0; k < 3; k++) lattice[index_lattice(i,j,k)] = new_spin[k];
                // get new action
                S_site(lattice, action_param_n, i, j, dSij_new);

                //change in action
                // action = - sum beta_i S_i  summing over action terms
                // dS = old - new due to - sign in definition of action
                double dS =  dotProduct(action_param_beta, dSij_old, num_terms) 
                            -dotProduct(action_param_beta, dSij_new, num_terms);

                //record deltaS
                deltaS[i*(ny*n_hits) + j*n_hits + n] = dS;

                // accept reject step 
                double random =  (double)rand() / (double)RAND_MAX; 
                if(random >= exp(-dS)){ // REJECT 
                    // REVERT lattice site 
                    for(int k = 0; k < 3; k++) lattice[index_lattice(i,j,k)] = old_spin[k];
                }

                // if accept, keep lattice updated and increment accept_rate
                else avg_metrop_accept_rate[0] += num_metrop_steps_inv;
            
            
            } // end loop over hits
        } // end j loop
    } // end i loop

    // save deltaS to file 
    //FILE* dsf;
    //char filename[64];
    //sprintf(filename, "dS/deltaS_N20k_%d.txt", iter);
    //dsf = fopen(filename, "w+");
    //for(int i = 0; i < n_hits*nx*ny; i++){
    //    fprintf(dsf, "%f ", deltaS[i]);
    //}
    //fclose(dsf);

    return;
}




void microcanonical_demon_update(double* avg_accept, double num_demon_steps_inv,  double* last_steps, double* lattice, double* action_param_beta, int* action_param_n, double epsilon, int file_name_index, int system_cycle){
    /*
    Do sweep of spin update and compute dS_alpha, the change in the action terms 
    for each term due to that spin update. Joint system S = sum beta_alpha (d_alpha - S_alpha)
    action remains the same -> 
    update  S_alpha -> S_alpha + dS_alpha 
            d_alpha -> d_alpha + dS_alpha
    
    save demon chain to file for each canonical sweep 
    */

    //record the entire demon chain for this sweep, then save this to file
    double demon_chain_current_sweep[num_terms*(nx*ny+1)]; // plus 1 for initial

    //buffer for "current" demon value to be updated
    double d_current[num_terms]; // add dS to get d_new[a] and see if in range
    

    // if this is the first set of demons, fill with demons set in middle of range
    if(file_name_index < num_demon_systems){
        for(int a = 0; a < num_terms; a++){
            demon_chain_current_sweep[a] = 0.5*(d_max - d_min) + d_min;
            d_current[a] = demon_chain_current_sweep[a];
        }
    }

    

    //otherwise, take the initial step in this chain to be the last step in the 
    //previous chain of the same system
    else{
        int index_system_last_previous = (file_name_index - num_demon_systems)*num_terms;
        for(int a = 0; a < num_terms; a++){
            demon_chain_current_sweep[a] = last_steps[index_system_last_previous  + a]; 
            d_current[a] = demon_chain_current_sweep[a];
        }
    }


    // buffer for next in chain 
    double d_new[num_terms]; // fill at each ij site

    // place holder for single term to compare to d_min, d_max
    double d_new_a; // d_current[a] + dS[a] may or may not fit in backpack 
    
    bool fit_in_backpack; // true or false determines whether to accept a demon update
    
    // save a copy of the lattice to modify here
    double* demon_lattice = lattice;
    
    double dSij_old[num_terms]; //contains action terms before a flip
    double dSij_new[num_terms]; //contains action terms after a flip
    double old_spin[3]; //holds value of local spin before
    double new_spin[3]; //holds value of local spin after 

    double dS[num_terms]; //dSij_new - dSij_old
    
    //sweep over lattice to get nx*ny more demon values 
    for(int i = 0; i < nx; i++){
        for(int j = 0; j < ny; j++){
        
            // get initial spin state (in case need to flip back)
            for(int k = 0; k < 3; k++) old_spin[k] = demon_lattice[index_lattice(i,j,k)];
            //get initial action
            S_site(demon_lattice, action_param_n, i, j, dSij_old);

            // get new spin state
            epsilon_update(old_spin, epsilon, new_spin);
            
            // update lattice site 
            for(int k = 0; k < 3; k++) demon_lattice[index_lattice(i,j,k)] = new_spin[k];
            // get new action
            S_site(demon_lattice, action_param_n, i, j, dSij_new);

            // change in action term 
            fit_in_backpack = true; //resets this to true for each ij site 
            for(int a = 0; a < num_terms; a++){
                dS[a] = dSij_new[a] - dSij_old[a];
                
                //calculat new demon value
                d_new_a = d_current[a] + dS[a]; 
                // decide if they all fit in backpack
                if(d_new_a < (double)d_min){
                    fit_in_backpack = false;
                }
                if(d_new_a > (double)d_max){
                    fit_in_backpack = false;
                }
            }
             
            // if fit, update d_new and spin (already updated)
            if(fit_in_backpack == true){
                avg_accept[0] += num_demon_steps_inv;
                for(int a = 0; a < num_terms; a++){
                    d_new[a] = d_current[a] + dS[a];
                }
                
            }
            //otherwise d_new = d_old and revert spin
            else{
                for(int a = 0; a < num_terms; a++){
                    d_new[a] = d_current[a];        
                }
                for(int k = 0; k < 3; k++) demon_lattice[index_lattice(i,j,k)] = old_spin[k];
            }
            
            // put new demon at the end of the chain 
            int current_index = (i*ny + j + 1)*num_terms; //plus 1 for initial
            for(int a = 0; a < num_terms; a++) demon_chain_current_sweep[current_index + a] = d_new[a]; 

            // "new" demon becomes current demon for next ij site
            for(int a = 0; a < num_terms; a++) d_current[a] = d_new[a];
            
            
        } // end j loop
    } // end i loop
            
   
    
        
    //record demons in this sweep
    FILE* fp_demon;
    char file_name_demon[64];
    sprintf(file_name_demon, "demons_systems_N%i/demon_file_%d.txt", iterations, file_name_index);
    fp_demon = fopen(file_name_demon, "w+");

    //save chain to file
    for(int i = 0; i < nx*ny + 1; i++){
        for(int a = 0; a < num_terms; a++){
            fprintf(fp_demon, "%0.5f ", demon_chain_current_sweep[i*num_terms + a]);
        }
        fprintf(fp_demon, "\n");
    } 
    fclose(fp_demon);
    
    
    //save last step in this chain to last_steps to be used as initial 
    //step in next sweep for this system (index is nx*ny since there are nx*ny+1 total)
    for(int a = 0; a < num_terms; a++) last_steps[file_name_index*num_terms +a] = demon_chain_current_sweep[nx*ny*num_terms + a];
        
    return;
}





void measure_corr(double* lattice, int measurement_num){
    /*
    Measure two point correlation function of given range of distance separation.
    Assume square lattice.
    */
    //int num_at_radius[radial_extent];
    //for(int d = 0; d < radial_extent; d++) num_at_radius[d] = 0;
    //int radii[radial_extent];

    // record observations at each distance here 
    double observations[nx];
    for(int d = 0; d < nx; d++) observations[d] = 0.0;

    for(int i = 0; i < nx; i++){
        for(int j = 0; j < ny; j++){
            //get local spin
            double local_site[3];
            double remote_site[3];
            for(int k = 0; k < 3; k++) local_site[k] = lattice[index_lattice(i,j,k)];
            
            //loop over horizontal displacement
            for(int dx = 0; dx < nx; dx++){
                //get spin at this remote position
                for(int k = 0; k < 3; k++) remote_site[k] = lattice[index_lattice((i+dx)%nx,j,k)];
                // record observation of correlation 
                // each point contributed 4 measurements at displacement d
                observations[dx] += dotProduct(local_site, remote_site, 3)/((double)(4*nx*ny));                
            } //end dx loop

            //loop over vertical displacement
            for(int dy = 0; dy < ny; dy++){
                //get spin at this remote position
                for(int k = 0; k < 3; k++) remote_site[k] = lattice[index_lattice(i,(j+dy)%ny,k)];
                // record observation of correlation 
                // each point contributed 4 measurements at displacement d
                observations[dy] += dotProduct(local_site, remote_site, 3)/((double)(4*nx*ny));                
            } //end dy loop 
        
        }// end j loop
    } //end i loop
    
    FILE *fp;
    char filename[64];
    sprintf(filename, "correlation_data_N%i/correlations_c_it%d.txt", iterations, measurement_num);
    fp = fopen(filename, "w+");

    //assumes square
    for(int d = 0; d < nx; d++){
        observations[d] = observations[d];        
        fprintf(fp,"%0.3f\n",observations[d]); 
    }
    fclose(fp);
    return;
}


