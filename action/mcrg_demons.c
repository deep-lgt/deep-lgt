#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <time.h>
#include "mcrg.h"
#include "spins_action.h"
#define PI 3.14159265
#define nx 16
#define ny 16
#define d_min 0
#define d_max 10
#define num_terms 2
#define iterations 1000000 // how many total metrop sweeps
#define num_demon_systems 100 // should be about autocorelation time 


int main(void) {


    /*     PARAMETER DEFINITION   */
    /*  ######################### */
    //double action_param_beta[12] = {1.30,0.35,0.01,0.02,0.004,-0.200
    //                            -0.080,-0.02,-0.01,-0.005,0.02,0.01};
    //int action_param_n[12] = {1,1,1,1,1,2,2,2,2,2,3,3}; 

    double action_param_beta[2] = {1.30, 0.35};
    int action_param_n[2] = {1, 1};

    //srand(time(NULL)); // set random number seed
    srand(13);
    double epsilon = 0.05;   // how large of a perturbation for each spin during updates
    int report = 1000;       // how many metrop sweeps have been completed 
    int n_hits = 5;         // how many hits on a spin during metrop before moving to next spin
    int thermal_iter = 999;  // how many metrop sweeps before record corr function and start demons
    int measure_iter = 20;  // how often to meaure correlation function
    int demon_iter = 20;     // how often to do microcanonical update between independent systems ^^ num_systems defined above

    double lattice[nx*ny*3]; // nx by ny lattice of spins with 3 degrees of freedom rotation
    

    
    /*      METROPOLIS SETUP      */
    /*  ######################### */
    random_lattice(lattice); // call function to fill lattice with random spins

    double deltaS[nx*ny*n_hits]; // save every dS computed in metrop algo here
    // deltaS[i*(ny*n_hits) + j*n_hits + n] indexing for nth of n_hits

    double avg_metrop_accept_rate[1] = {0.0};
    double num_metrop_steps_inv = (double) 1/ (double) (iterations*nx*ny*n_hits);
    // ^^ avg_accept += num_metrop_steps_inv if accept metrop update, no need to divide at the end

    int measurement_num = 0;// for filenames of correlation functions at each measurement
    /*  ######################### */
  



    /* MICROCANONICAL DEMON SETUP */
    /*  ######################### */
    int canonical_num = 0;  // for filenames of demon chains  
    int system_cycle = 0;   // keep track of how many times through the demon system 
    
                            // how many demon sweeps will be completed 
    int total_demon_sweeps = (int) (iterations - thermal_iter-1)/demon_iter;
    
    double avg_accept[1] = {0.0};   // record average acceptance rate within microcanonical sweeps 
    double num_demon_steps_inv = (double) 1/ (double) (total_demon_sweeps*ny*ny);
    // ^^ avg_accept += num_demon_steps_inv if accept demon update, no need to divide at the end

    // record here the last step in a chain during microcanonical sweep so can pick up system where it 
    // left off 
    double last_steps[num_terms*total_demon_sweeps];  
    /*  ######################### */



    
    /*  ######################### */
    /*          ITERATE           */
    // complete # iterations metropolis sweeps, microcanonical updates, and corr function measurements
    for(int iter = 0; iter < iterations; iter ++){
        if(iter%report == 0) printf("%d sweeps completed out of %d\n", iter, iterations);
        
        // complete one metropolis sweep to update lattice 
        metropolis_sweep(lattice, deltaS, avg_metrop_accept_rate, action_param_beta, action_param_n, n_hits, epsilon, iter, num_metrop_steps_inv); 
        

        
        // complete a microcanonical demon sweep DOES NOT AFFECT LATTICE CHAIN
        if(iter> thermal_iter && iter%demon_iter==0){
            microcanonical_demon_update(avg_accept, num_demon_steps_inv, last_steps, lattice, action_param_beta, action_param_n, epsilon, canonical_num, system_cycle);
            canonical_num += 1; //update the file index so demon chain file can be saved for each sweep 
            // if iterated through num_demon_systems, update cycle number will work on next 
            //used for indexing last_steps (get demons from previous cycle to start each cycle)
            if(canonical_num%num_demon_systems == 0) system_cycle += 1; 
        }
    
        



    
        //record correlation function
        if(iter > thermal_iter && iter%measure_iter == 0){
            //call correlation calculator
            measure_corr(lattice, measurement_num);
            measurement_num += 1; //update the file index so each corr can be saved 
        }


    
    } // end iterations loop 
    


    
    /*      GET RESULTS           */ 
    /*  ######################### */

    //printf("avg demon accepts rate = %f\n", avg_accept[0]);
    printf("avg metropolis accepts rate = %f\n", avg_metrop_accept_rate[0]);





    return 0;

}


















