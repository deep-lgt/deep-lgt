import numpy as np
import matplotlib.pyplot as plt


def map_vec(a, b):
    """
    Map two numbers (or two 1d numpy array of same lengths), a and b, in [0, 1)
    to a vector [x, y, z] with unit norm.
    If a and b are randomly distributed in [0, 1) interval, the function will
    return randomly distributed vectors.
    """
    psi = 2*np.pi*a # azimuthal angle
    phi = np.arccos(2*b-1) # polar angle
    return np.array([np.sin(phi)*np.cos(psi),
            np.sin(phi)*np.sin(psi),
            np.cos(phi)])


def axis_rot(v, w):
    """
    Given a fixed unit vector v in cartesian coordinates [x,y,z],
    rotate vector w such that if w == [0, 0, 1], the function will output
    vector v. This is done by using Rodrigues' rotation formula.
    """
    # Normalize v
    v = np.array(v)/np.linalg.norm(v)
    # Find the axis of rotation, k
    k = np.cross(np.array([0,0,1]), v)
    k = k/np.linalg.norm(k)
    # Find angle of rotation, phi
    cosphi = np.dot(v, np.array([0,0,1]))
    phi = np.arccos(cosphi)
    # Apply rotation formula
    wrot = w*cosphi + np.cross(k, w)*np.sin(phi) + k*np.dot(k, w)*(1-cosphi)
    return wrot


def epsilon_update(v, epsilon):
    """
    Given a vector with unit norm, return a new vector, w, such that w is chosen
    randomly around the cone centered at vector v.
    epsilon is in the interval [0, 1) and the angle of the cone is given
    by arccos(2*(1-epsilon) - 1).
    """
    # First find the new random vector around a cone centered at vector [0, 0, 1]
    nv = map_vec(np.random.rand(), # random azimuthal angle
                 1-np.random.rand()*epsilon)
    # Rotate this vector
    return axis_rot(v, nv)


def random_lattice(nx, ny):
    """
    Return a random numpy array lattice with shape [3, nx, ny] where
    the first index gives the spin component sx, sy, sz at a lattice site.
    """
    l = map_vec(np.random.rand(nx*ny),
                np.random.rand(nx*ny))
    return np.reshape(l, [3, nx, ny])


# From M. Hasenbusch et al. Physics Letter B 338 (1994) 308-312
# Table 1 and second column of table 2
# beta_i, list of v, n
"""
action_param = \
[
[1.30  , [[1,0],[0,1]] , 1],
[0.35  , [[1,1],[1,-1]] , 1],
[0.01  , [[2,0],[0,2]] , 1],
[0.02  , [[2,1],[2,-1],[1,2],[1,-2]], 1],
[0.004 , [[3,0],[0,3]] , 1],
[-0.200, [[1,0],[0,1]] , 2],
[-0.080, [[1,1],[1,-1]], 2],
[-0.02 , [[2,0],[0,2]] , 2],
[-0.01 , [[2,1],[2,-1],[1,2],[1,-2]], 2],
[-0.005, [[3,0],[0,3]] , 2],
[0.02  , [[1,0],[0,1]] , 3],
[0.01  , [[1,1],[1,-1]], 3],
]
"""

# 6 terms truncation for 16 by 16 lattice
action_param = \
[
[1.1408  , [[1,0],[0,1]] , 1],
[0.3014  , [[1,1],[1,-1]] , 1],
[-0.0017  , [[2,0],[0,2]] , 1],
[0.0149  , [[2,1],[2,-1],[1,2],[1,-2]], 1],
[0.0022 , [[3,0],[0,3]] , 1],
]


def Si(in_lat, in_param, site_tuple, dir=1):
    """
    Calculate the lattice action associated with site (xi, xj). dir can be
    1 or -1 which calculate the non-ovelaping part of the action the involve the
    spin at site. The sum of those two contributions give the total action
    that involve spin at that site.
    """
    nx, ny = (np.shape(in_lat))[1:]
    ds = 0
    # Loop over interaction terms
    for i, si in enumerate(in_param):
        # Loop over all interaction neighbors
        for vi in si[1]:
            ns = [(site_tuple[0]+dir*vi[0])%nx,
                  (site_tuple[1]+dir*vi[1])%ny]

            ds += 0.5 * si[0] * \
                  (np.dot(in_lat[:, site_tuple[0], site_tuple[1]],
                          in_lat[:, ns[0], ns[1]]))**si[2]
    return -ds


def Stot(in_lat, in_param):
    """
    Return the action for a given lattice and action parameters.
    Here we assume periodic boundary condition.
    """
    nx, ny = (np.shape(in_lat))[1:]
    all_sites = np.unravel_index(np.arange(nx*ny), [nx, ny])
    accum_s = 0.0

    # Loop over lattice sites
    for site_tuple in zip(all_sites[0], all_sites[1]):
        accum_s += Si(in_lat, in_param, site_tuple, dir=1)
    return accum_s


def metropolis_sweep(in_lat, in_param, n, epsilon):
    """
    Perform a sweep of metropolis update to the input lattice. Each site will be
    updated n times regardless of being accepted or rejected.
    epsilon is in [0, 1) that controls how big the proposed updates will be with
    0 being identity (no changes in updates).
    """
    nx, ny = (np.shape(in_lat))[1:]
    all_sites = np.unravel_index(np.arange(nx*ny), [nx, ny])
    accp_no = 0
    rejt_no = 0

    # Loop over lattice sites
    for site_tuple in zip(all_sites[0], all_sites[1]):
        # Multi-hit updates
        for ni in range(n):
            # Compute old action
            old_lat = np.array(in_lat, copy=True)
            old_S = Si(old_lat, in_param, site_tuple, dir=1) +\
                    Si(old_lat, in_param, site_tuple, dir=-1)

            # Propose an update
            new_v = epsilon_update(old_lat[:, site_tuple[0], site_tuple[1]],
                                   epsilon)
            in_lat[:, site_tuple[0], site_tuple[1]] = new_v

            # Accept and reject step
            new_S = Si(in_lat, in_param, site_tuple, dir=1) +\
                    Si(in_lat, in_param, site_tuple, dir=-1)
            dS = new_S - old_S
            if np.exp(-dS) < np.random.rand():
                in_lat = old_lat # revert back changes
                rejt_no += 1
                continue
            accp_no += 1
    return (in_lat, accp_no, rejt_no)

def measure_corr(in_lat, corr_range):
    """
    Measure two point correlation function of given range of distance separation.
    Assume square lattice.
    """
    nx, ny = (np.shape(in_lat))[1:]
    obs = np.zeros(len(corr_range))
    for yi in range(ny):
        obs += np.array([np.dot(in_lat[:, 0, yi], in_lat[:, ri%nx, yi])
                         for ri in corr_range])
        obs += np.array([np.dot(in_lat[:, yi, 0], in_lat[:, yi, ri%ny])
                         for ri in corr_range])

    return  obs/(ny+ny)


def run():
    """run me."""
    seed = 2021
    np.random.seed(seed)

    nx = 16 # lattice size. use square lattice
    ny = nx
    iterations = 1000 # number of metropolis update sweeps of whole lattice
    n_hits = 10 # how many metropolis update to perform at each site
    epsilon = 0.3 # between 0 and 1, the size of metropolis update (0 = no changes)
    thermal_iter = 50 # number of thermalization iterations to perform before measuring
    measure_iter = 2 # how frequent to take in measurement
    save_name = "data_2.npz" # data save name


    # Do work
    in_lat = random_lattice(nx, ny) # initial random lattice
    accp_no = 0
    rejt_no = 0
    obs_list = []
    for i in range(iterations):
        in_lat, ia, ir = metropolis_sweep(in_lat, action_param, n_hits, epsilon)
        accp_no += ia
        rejt_no += ir
        print("iteration = %s, total hits = %i, accptance rate = %.3f"
              %(i, accp_no+rejt_no, accp_no/(accp_no+rejt_no)))

        if i > thermal_iter and i%measure_iter == 0: # thermalization finished
            obs = measure_corr(in_lat, np.arange(nx))
            obs_list.append(obs)

    obs_list = np.array(obs_list)
    data_mean = np.mean(obs_list, axis=0)
    data_std = np.sqrt(np.var(obs_list, axis=0)/(np.shape(obs_list))[0])
    np.save(save_name, obs_list)

    # Plot results
    fig, ax = plt.subplots(1,1,figsize=(7, 5))
    ax.set_yscale('log')
    ax.errorbar(np.arange(np.size(data_mean)),y=np.abs(data_mean),
                yerr=np.abs(data_std),
                marker='o',
                color="#1f77b4",
                ls='none')
    plt.show()

if __name__ == "__main__":
    run()
