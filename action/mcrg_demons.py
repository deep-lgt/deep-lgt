import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sb
import os 
from scipy.optimize import fsolve

def map_vec(a, b):
    """
    Map two numbers (or two 1d numpy array of same lengths), a and b, in [0, 1)
    to a vector [x, y, z] with unit norm.
    If a and b are randomly distributed in [0, 1) interval, the function will
    return randomly distributed vectors on the sphere.
    """
    psi = 2*np.pi*a # azimuthal angle
    phi = np.arccos(2*b-1) # polar angle taking into acount jacobian to ensure random on sphere
    return np.array([np.sin(phi)*np.cos(psi),
            np.sin(phi)*np.sin(psi),
            np.cos(phi)])




def axis_rot(v, w):
    """
    Given a fixed unit vector v (one of our spins) in cartesian coordinates [x,y,z],
    rotate vector w such that if w == [0, 0, 1], the function will output
    vector v. This is done by using Rodrigues' rotation formula.
   
    We will use a perturbation of w = [0,0,1] to get a perturbation of v 
    """
    # Normalize v
    v = np.array(v)/np.linalg.norm(v)
    # Find the axis of rotation, k
    k = np.cross(np.array([0,0,1]), v)
    k = k/np.linalg.norm(k)
    # Find angle of rotation, phi
    cosphi = np.dot(v, np.array([0,0,1]))
    phi = np.arccos(cosphi)
    # Apply rotation formula
    wrot = w*cosphi + np.cross(k, w)*np.sin(phi) + k*np.dot(k, w)*(1-cosphi)
    return wrot





"""
def plot_Rodrigues(theta,phi):
    v = np.array([np.sin(theta)*np.cos(phi), np.sin(theta)*np.sin(phi), np.cos(theta)])
    w = np.array([0,0,1])
    w_new = axis_rot(v,w)
    
    origin = [0,0,0]
    X, Y, Z = zip(origin,origin,origin) 
    A,B,C = zip(v,v,w_new)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.quiver(X,Y,Z,A,B,C)
    plt.show()
theta = 0.5*np.pi
phi = 0.25*np.pi
plot_Rodrigues(theta,phi)
"""


def epsilon_update(v, epsilon):
    """
    Given a vector with unit norm, return a new vector, w, such that w is chosen
    randomly around the cone centered at vector v.
    epsilon is in the interval [0, 1) and the angle of the cone is given
    by arccos(2*(1-epsilon) - 1).
    """
    # First find the new random vector around a cone centered at vector [0, 0, 1]
    a = 0.11
    b = 0.22
    nv = map_vec(a, 1-b*epsilon)
    #nv = map_vec(np.random.rand(), # random azimuthal angle
    #             1-np.random.rand()*epsilon)
    # Rotate this vector
    return axis_rot(v, nv)

v = [1,2,3]
epsilon = 0.1
newv = epsilon_update(v,epsilon)
print(newv)

def random_lattice(nx, ny):
    """
    Return a random numpy array lattice with shape [3, nx, ny] where
    the first index gives the spin component sx, sy, sz at a lattice site.
    """
    l = map_vec(np.random.rand(nx*ny),
                np.random.rand(nx*ny))
    return np.reshape(l, [3, nx, ny])


# From M. Hasenbusch et al. Physics Letter B 338 (1994) 308-312
# Table 1 and second column of table 2
# beta_i, list of v, n

action_param = \
[
[1.30  , [[1,0],[0,1]] , 1],
[0.35  , [[1,1],[1,-1]] , 1],
[0.01  , [[2,0],[0,2]] , 1],
[0.02  , [[2,1],[2,-1],[1,2],[1,-2]], 1],
[0.004 , [[3,0],[0,3]] , 1],
[-0.200, [[1,0],[0,1]] , 2],
[-0.080, [[1,1],[1,-1]], 2],
[-0.02 , [[2,0],[0,2]] , 2],
[-0.01 , [[2,1],[2,-1],[1,2],[1,-2]], 2],
[-0.005, [[3,0],[0,3]] , 2],
[0.02  , [[1,0],[0,1]] , 3],
[0.01  , [[1,1],[1,-1]], 3],
]
"""

# 6 terms truncation for 16 by 16 lattice
action_param = \
[
[1.1408  , [[1,0],[0,1]] , 1],
[0.3014  , [[1,1],[1,-1]] , 1],
[-0.0017  , [[2,0],[0,2]] , 1],
[0.0149  , [[2,1],[2,-1],[1,2],[1,-2]], 1],
[0.0022 , [[3,0],[0,3]] , 1],
]
"""

def Si(in_lat, in_param, site_tuple, dir=1):
    """
    Calculate the lattice action associated with site (xi, xj). dir can be
    1 or -1 which calculate the non-ovelaping part of the action the involve the
    spin at site. The sum of those two contributions give the total action
    that involve spin at that site.
    """
    nx, ny = (np.shape(in_lat))[1:]
    ds_alpha = [0 for each in in_param] #list of changes in each action term
    # Loop over interaction terms
    for i, si in enumerate(in_param):
        # Loop over all interaction neighbors
        for vi in si[1]:
            ns = [(site_tuple[0]+dir*vi[0])%nx,
                  (site_tuple[1]+dir*vi[1])%ny]

            ds_alpha[i] -= 0.5 * si[0] * \
                  (np.dot(in_lat[:, site_tuple[0], site_tuple[1]],
                          in_lat[:, ns[0], ns[1]]))**si[2]

    #total change in action is sum over change in each action term        
    return ds_alpha


def Stot(in_lat, in_param):
    """
    Return the action for a given lattice and action parameters.
    Here we assume periodic boundary condition.
    """
    nx, ny = (np.shape(in_lat))[1:]
    all_sites = np.unravel_index(np.arange(nx*ny), [nx, ny])
    accum_s_alpha = [0.0 for each in in_param]

    # Loop over lattice sites
    for ite_tuple in zip(all_sites[0], all_sites[1]):
        ds_alpha = Si(in_lat, in_param, site_tuple, dir = 1)
        
        #add up the total change in each action term
        for i in enumerate(in_param):
            accum_s_alpha[i] +=  ds_alpha[i]
    
    return accum_s_alpha


def metropolis_sweep(in_lat, in_param, n, epsilon):
    """
    Perform a sweep of metropolis update to the input lattice. Each site will be
    updated n times regardless of being accepted or rejected.
    epsilon is in [0, 1) that controls how big the proposed updates will be with
    0 being identity (no changes in updates).
    """
    nx, ny = (np.shape(in_lat))[1:]
    all_sites = np.unravel_index(np.arange(nx*ny), [nx, ny])
    accp_no = 0
    rejt_no = 0

    # Loop over lattice sites
    for site_tuple in zip(all_sites[0], all_sites[1]):
        # Multi-hit updates
        for ni in range(n):
            # Compute old action by summing old action terms
            old_lat = np.array(in_lat, copy=True)
            old_S = sum(Si(old_lat, in_param, site_tuple, dir=1)) +\
                    sum(Si(old_lat, in_param, site_tuple, dir=-1))

            # Propose an update
            new_v = epsilon_update(old_lat[:, site_tuple[0], site_tuple[1]],
                                   epsilon)
            in_lat[:, site_tuple[0], site_tuple[1]] = new_v
                
            #compute new action by summing action terms
            new_S = sum(Si(in_lat, in_param, site_tuple, dir=1)) +\
                    sum(Si(in_lat, in_param, site_tuple, dir=-1))
            
            # Accept and reject step
            dS = new_S - old_S
            if np.exp(-dS) < np.random.rand():
                #revert to old lattice
                in_lat = old_lat 
                rejt_no += 1
                continue
            accp_no += 1
    return (in_lat, accp_no, rejt_no)




def microcanonical_demon_update(in_d_alpha, in_lat, in_param, epsilon, d_min, d_max):
    """
    Do one random spin update and compute dS_alpha, the total change in the action 
    for each term due to that spin update. Joint system S = sum beta_alpha (d_alpha - S_alpha)
    action remains the same -> 
    update  S_alpha -> S_alpha + dS_alpha 
            d_alpha -> d_alpha + dS_alpha
    """

    rejt_no = 0
    nx, ny = (np.shape(in_lat))[1:]
    #get a random site to update
    site_tuple = [np.random.randint(0,nx), np.random.randint(0,ny)]

    # Compute old action terms
    old_lat = np.array(in_lat, copy=True)
    old_S_alpha =[  Si(old_lat, in_param, site_tuple, dir=1)[i] +\
                    Si(old_lat, in_param, site_tuple, dir=-1)[i] for i in range(len(in_param))]

    # Propose an update
    new_v = epsilon_update(old_lat[:, site_tuple[0], site_tuple[1]],
                            epsilon)
    in_lat[:, site_tuple[0], site_tuple[1]] = new_v

    # Compute new action terms
    new_S_alpha = [ Si(in_lat, in_param, site_tuple, dir=1)[i] +\
                    Si(in_lat, in_param, site_tuple, dir=-1)[i] for i in range(len(in_param))]
    
    dS_alpha = [new_S_alpha[i] - old_S_alpha[i] for i in range(len(in_param))]
    
    old_d_alpha = in_d_alpha

    
    print(dS_alpha)
    #print(in_d_alpha)
    #attempt to update d_alpha
    fit_in_backpack = True
    for i in range(len(in_param)):
        in_d_alpha[i] += dS_alpha[i]
        if in_d_alpha[i] < d_min:
            fit_in_backpack = False
        if in_d_alpha[i] > d_max:
            fit_in_backpack = False

    if fit_in_backpack == False:
        print("reject demon step")
        #revert to old d_alpha and old_lat
        in_d_alpha = old_d_alpha    
        in_lat = old_lat
        rejt_no  = 1
    
    #print(in_d_alpha)
    return (in_d_alpha, in_lat)




def measure_corr(in_lat, corr_range):
    """
    Measure two point correlation function of given range of distance separation.
    Assume square lattice.
    """
    nx, ny = (np.shape(in_lat))[1:]
    observation = np.zeros(len(corr_range))
    for yi in range(ny):
        observation += np.array([np.dot(in_lat[:, 0, yi], in_lat[:, ri%nx, yi])
                         for ri in corr_range])
        observation += np.array([np.dot(in_lat[:, yi, 0], in_lat[:, yi, ri%ny])
                         for ri in corr_range])

    return  observation/(ny+ny)


def plot_lattice_z(lattice,i):
    z_components = lattice[2][:][:]
    #sb.palplot(sb.color_palette("Blues"))
    heat_map = sb.heatmap(z_components,cmap="YlGnBu")
    plt.savefig("%03d_heatmap.png"%i)
    plt.close()

def run():
    """run me."""
    seed = 2021
    np.random.seed(seed)

    nx = 16 # lattice size. use square lattice
    ny = nx
    demon_chain = [[5] for each in action_param] #initial demon chain
    d_min = 0
    d_max = 10
    iterations = 200 # number of metropolis update sweeps of whole lattice
    n_hits = 5 # how many metropolis update to perform at each site
    epsilon = 0.3 # between 0 and 1, the size of metropolis update (0 = no changes)
    thermal_iter = 20 # number of thermalization iterations to perform before measuring
    measure_iter = 5 # how frequent to take in measurement determine from autocorrelation time 
    demon_iter = 5 #time between demon updates
    plot_iter = 5
    save_name = "data_2.npz" # data save name


    # Do work
    in_lat = random_lattice(nx, ny) # initial random lattice
    accp_no = 0
    rejt_no = 0
    plot_index = 0 #saved plots labeled as plot_index_heatmap.png
    observation_list = []
    for i in range(iterations):
        in_lat, ia, ir = metropolis_sweep(in_lat, action_param, n_hits, epsilon)
        
        #update demons
        if i>thermal_iter and i%demon_iter==0:
            in_demon_alpha = [demon[-1] for demon in demon_chain] #get last value for each alpha  
            new_d_alpha, in_lat = microcanonical_demon_update(in_demon_alpha, in_lat, action_param, epsilon, d_min, d_max)
            #add the new value of each 
            for j in range(len(action_param)):
                demon_chain[j].append(new_d_alpha[j])
        
        #plot 
        #if i%plot_iter == 0:
        #    plot_lattice_z(in_lat,plot_index)
        #    plot_index += 1

        accp_no += ia
        rejt_no += ir
        if i%20==0:
            print("iteration = %s, total hits = %i, accptance rate = %.3f"
                    %(i, accp_no+rejt_no, accp_no/(accp_no+rejt_no)))

        #get correlation function
        if i > thermal_iter and i%measure_iter == 0: # thermalization finished
            observation = measure_corr(in_lat, np.arange(nx))
            observation_list.append(observation)

    observation_list = np.array(observation_list)
    data_mean = np.mean(observation_list, axis=0)
    data_std = np.sqrt(np.var(observation_list, axis=0)/(np.shape(observation_list))[0])
    np.save(save_name, observation_list)

    np.save("demon_chain",demon_chain)

    # Plot results
    fig, ax = plt.subplots(1,1,figsize=(7, 5))
    ax.set_yscale('log')
    ax.errorbar(np.arange(np.size(data_mean)),y=np.abs(data_mean),
                yerr=np.abs(data_std),
                marker='o',
                color="#1f77b4",
                ls='none')
    plt.show()
   

    #make a histogram of demon chain
    for j in range(len(action_param)):
        demons = demon_chain[j]
        plt.hist(demons,bins=100, range=((0,10)))
        plt.savefig("demon%i_hist.png"%j)
        plt.show()
        plt.close()

    

    #compute beta_alpha numerically 
    for j in range(len(action_param)):
        mean_demon = np.mean(demon_chain[j]) 
        func = lambda beta : 1/beta * (1- beta*(d_max-d_min)/(np.exp(beta*(d_max - d_min)) -1)) - mean_demon
        beta_initial_guess = 1.0
        beta_solution = fsolve(func, beta_initial_guess)
        print("beta_%i = %0.5f"%(j, beta_solution))



    #framenames = "_heatmap.png"
    #moviename = "heatmap.mp4"
    #os.system("ffmpeg -y -r 2 -i " + "%03d" +framenames + " -c:v libx264 -vf 'fps=25,format=yuv420p' " + moviename)
    #os.system("open %s"%moviename)
    
if __name__ == "__main__":
    #run()
    pass
