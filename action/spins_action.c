#include "spins_action.h"

int index_lattice(int i, int j, int k){
    // row major 
    return i*ny*3 + j*3 + k;
}

double dotProduct(double* vect_A, double* vect_B, int n) { 
    double product = 0; 
    for (int i = 0; i < n; i++) 
        product = product + vect_A[i] * vect_B[i]; 
    return product; 
}


void S_site(double* lattice, int* action_param_n, int site_i, int site_j, double* dSij){
    /*
    Calculate the lattice action associated with site (xi, xj).
    assume periodic boundary conditions - note that all terms 
    have a factor of 1/2 by their definition in Hasenbusch paper
    */
    
    // get local spin value
    double local_spin[3];
    for(int k = 0; k < 3; k++) local_spin[k] = lattice[index_lattice(site_i, site_j, k)];
    
    // Loop over interaction terms
    for(int a=0; a < num_terms; a++){
        int n = action_param_n[a];

        double neighbor1[3];
        double neighbor2[3];
        double neighbor3[3];
        double neighbor4[3];
        double neighbor5[3];
        double neighbor6[3];
        double neighbor7[3];
        double neighbor8[3];
        
        
        if(a == 0 || a == 5 || a == 10){ // v= (1,0)
            for(int k = 0; k < 3; k++){
                neighbor1[k] = lattice[index_lattice((site_i + 1 + nx)%nx, site_j, k)];
                neighbor2[k] = lattice[index_lattice((site_i - 1 + nx)%nx, site_j, k)];
                neighbor3[k] = lattice[index_lattice(site_i, (site_j + 1 + ny)%ny, k)];
                neighbor4[k] = lattice[index_lattice(site_i, (site_j - 1 + ny)%ny, k)];
            }
            dSij[a] =0.5*(pow(dotProduct(local_spin, neighbor1, 3), n) 
                        + pow(dotProduct(local_spin, neighbor2, 3), n)
                        + pow(dotProduct(local_spin, neighbor3, 3), n)
                        + pow(dotProduct(local_spin, neighbor4, 3), n));
        }
          
        if(a == 1 || a == 6 || a == 11){ // v= (1,1)
            for(int k = 0; k < 3; k++){
                neighbor1[k] = lattice[index_lattice((site_i + 1 + nx)%nx, (site_j + 1 + ny)%ny, k)];
                neighbor2[k] = lattice[index_lattice((site_i - 1 + nx)%nx, (site_j + 1 + ny)%ny, k)];
                neighbor3[k] = lattice[index_lattice((site_i + 1 + nx)%nx, (site_j - 1 + ny)%ny, k)];
                neighbor4[k] = lattice[index_lattice((site_i - 1 + nx)%nx, (site_j - 1 + ny)%ny, k)];
            }
            dSij[a] =0.5*(pow(dotProduct(local_spin, neighbor1, 3), n)
                        + pow(dotProduct(local_spin, neighbor2, 3), n)
                        + pow(dotProduct(local_spin, neighbor3, 3), n)
                        + pow(dotProduct(local_spin, neighbor4, 3), n));
        }
    
        if(a == 2 || a == 7){ // v= (2,0)
            for(int k = 0; k < 3; k++){
                neighbor1[k] = lattice[index_lattice((site_i + 2 + nx)%nx, site_j, k)];
                neighbor2[k] = lattice[index_lattice((site_i - 2 + nx)%nx, site_j, k)];
                neighbor3[k] = lattice[index_lattice(site_i, (site_j + 2 + ny)%ny, k)];
                neighbor4[k] = lattice[index_lattice(site_i, (site_j - 2 + ny)%ny, k)];
            }
            dSij[a] =0.5*(pow(dotProduct(local_spin, neighbor1, 3), n)
                        + pow(dotProduct(local_spin, neighbor2, 3), n)
                        + pow(dotProduct(local_spin, neighbor3, 3), n)
                        + pow(dotProduct(local_spin, neighbor4, 3), n));
        
        }
    
        if(a == 3 || a == 8){ // v= (2,1)
            for(int k = 0; k < 3; k++){
                neighbor1[k] = lattice[index_lattice((site_i + 2 + nx)%nx, (site_j + 1 + ny)%ny, k)];
                neighbor2[k] = lattice[index_lattice((site_i + 2 + nx)%nx, (site_j - 1 + ny)%ny, k)];
                neighbor3[k] = lattice[index_lattice((site_i - 2 + nx)%nx, (site_j + 1 + ny)%ny, k)];
                neighbor4[k] = lattice[index_lattice((site_i - 2 + nx)%nx, (site_j - 1 + ny)%ny, k)];
                neighbor5[k] = lattice[index_lattice((site_i + 1 + nx)%nx, (site_j + 2 + ny)%ny, k)];
                neighbor6[k] = lattice[index_lattice((site_i + 1 + nx)%nx, (site_j - 2 + ny)%ny, k)];
                neighbor7[k] = lattice[index_lattice((site_i - 1 + nx)%nx, (site_j + 2 + ny)%ny, k)];
                neighbor8[k] = lattice[index_lattice((site_i - 1 + nx)%nx, (site_j - 2 + ny)%ny, k)];
            }
            dSij[a] =0.5*(pow(dotProduct(local_spin, neighbor1, 3), n)
                        + pow(dotProduct(local_spin, neighbor2, 3), n)
                        + pow(dotProduct(local_spin, neighbor3, 3), n)
                        + pow(dotProduct(local_spin, neighbor4, 3), n)
                        + pow(dotProduct(local_spin, neighbor5, 3), n)
                        + pow(dotProduct(local_spin, neighbor6, 3), n)
                        + pow(dotProduct(local_spin, neighbor7, 3), n)
                        + pow(dotProduct(local_spin, neighbor8, 3), n));
        }

        if(a == 4 || a == 9){ // v= (3,0)
            for(int k = 0; k < 3; k++){
                neighbor1[k] = lattice[index_lattice((site_i + 3 + nx)%nx, site_j, k)];
                neighbor2[k] = lattice[index_lattice((site_i - 3 + nx)%nx, site_j, k)];
                neighbor3[k] = lattice[index_lattice(site_i, (site_j + 3 + ny)%ny, k)];
                neighbor4[k] = lattice[index_lattice(site_i, (site_j - 3 + ny)%ny, k)];
            }
            dSij[a] =0.5*(pow(dotProduct(local_spin, neighbor1, 3), n)
                        + pow(dotProduct(local_spin, neighbor2, 3), n)
                        + pow(dotProduct(local_spin, neighbor3, 3), n)
                        + pow(dotProduct(local_spin, neighbor4, 3), n));
        }
    }

    return;
}



void S_tot(double* lattice, double* action_param_beta, int* action_param_n, double* S){
    /*
    Return the action for a given lattice and action parameters.
    Here we assume periodic boundary condition.
    */

    // loop over sites
    for(int i = 0; i < nx; i++){
        for(int j = 0; j < ny; j++){
            double dSij[num_terms];
            // get the action term values at this site
            S_site(lattice, action_param_n, i, j, dSij);
            //sum up each site with factor of 1/2 for double counting 
            for(int a = 0; a < num_terms; a++) S[a] += 0.5*dSij[a];
        }
    }
    
    return;
}
