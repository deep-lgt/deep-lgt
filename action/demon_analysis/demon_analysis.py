import numpy as np
import os
from scipy.optimize import fsolve
import matplotlib.pyplot as plt

"""
fp = [np.loadtxt("correlation_data/correlations_c_it%i.txt"%i) for i in range(97)]
mean = [np.mean([fp[i][r] for i in range(len(fp))]) for r in range(len(fp[0]))]
std = [np.std([fp[i][r] for i in range(len(fp))]) for r in range(len(fp[0]))]
#print(mean)
#plt.errorbar([r for r in range(len(fp[0]))], mean, yerr = std, fmt = "--o")
#plt.savefig("corr1.png")
#plt.show()
"""



## CORRELATIONS?? 
"""
# GOT DATA ANALYZE BELOW
num_terms = 2
#minn = 2000
#maxx = 4000
minn = 0
maxx = 9900
lat_size = 256
#print(minn)
#print(maxx)
#print()
num_systems = 100

demon_by_time = [[[] for system in range(num_systems)] for term in range(num_terms)] 

for i in range(minn, maxx):
    system = i%num_systems
    fp_demon = np.loadtxt("demons_systems_N200000/demon_file_%d.txt"%i)
    N = len(fp_demon)-1 #dont use last because replicated as first 
    fp_demon = fp_demon[0:-1]
    demons_by_term = np.reshape(np.array(fp_demon), (num_terms, N))
    for a in range(num_terms):
        demon_by_time[a][system].append(np.mean(demons_by_term[a]))

np.save("demon_by_time", demon_by_time)

demon_by_time = np.load("demon_by_time.npy")
n = int(maxx/num_systems)
times = [each for each in range(n)]
term = 0
for system in range(20):#(num_systes):
    y = [demon_by_time[0][system][i] + system*10 for i in range(n)]
    plt.plot(times, y)
#for sweep in range(int(maxx/num_systems)):
#    plt.axvline(x = sweep*lat_size, color = 'black')
plt.show()
"""







num_terms = 2
#minn = 2000
#maxx = 4000
minn = 0
maxx = 49950
"""
demons = [[] for a in range(num_terms)]

for i in range(minn, maxx):
    fp_demon = np.loadtxt("demons_systems_N1000000/demon_file_%d.txt"%i)
    N = len(fp_demon)-1 #dont use last because replicated as first 
    fp_demon = fp_demon[0:-1]

    demons_by_term = np.reshape(np.array(fp_demon), (num_terms, N))
    for a in range(num_terms):
        demons[a].extend(demons_by_term[a])

np.save("demon_analysis/demons_all_systems_N1000000", demons)
print(len(demons))
print(len(demons[0]))
"""
demon_chain = np.load("demon_analysis/demons_all_systems_N1000000.npy")

for i in range(len(demon_chain)):
    demon = demon_chain[i]
    print(len(demon))
    print([min(demon), max(demon)])
    plt.hist(demon, range = (0,10), bins = 50)
    plt.title("alpha = %i"%(i+1))
    plt.savefig("demon_analysis/N200000_demon_alpha%i.png"%i)
    plt.show()
    plt.close()



d_min = 0
d_max = 10

def func(beta):
    num = beta*(d_max - d_min)
    den = np.exp(beta*(d_max - d_min)) - 1
    return 1/beta * (1 - num/den)

hasenbusch_params = [1.3, 0.35, 0.01, 0.02, 0.004, -0.2, -0.08, -0.02, -0.01, -0.005, 0.02, 0.01]

for j in range(len(demon_chain)):
    #print(j)
    mean_demon = np.mean(demon_chain[j])
    #means[j].append(mean_demon)
    print("true mean = %f"%func(hasenbusch_params[j]))
    print("emp  mean = %f"%mean_demon)
    print()


#for i in range(num_terms):
#    plt.hist(means[i])
#    plt.show()

#print(means)
#    print(mean_demon)
#    func = lambda beta : 1/beta * (1- beta*(d_max-d_min)/(np.exp(beta*(d_max - d_min)) -1)) - mean_demon
#    beta_initial_guess = 1.0
#    beta_solution = fsolve(func, beta_initial_guess)
#    print("beta_%i = %0.5f"%(j, beta_solution))
#    print()

#for i in range(10):
#    os.system("rm demons_systems_N200000_1term/*%0i.txt"%i)

