#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <time.h>
#define PI 3.14159265
#define nx 16
#define ny 16
#define d_min 0
#define d_max 10
#define num_terms 12
#define num_demon_systems 10 // should be about autocorelation time 

void  map_vec(double a, double b, double* vector); 
double dotProduct(double vect_A[], double vect_B[], int n);
void crossProduct(double* vect_A, double* vect_B, double* vector); 
void axis_rot(double* v, double* w, double* vector);
void epsilon_update(double* v, double epsilon, double* new_v);
int index_lattice(int i, int j, int k);
void random_lattice(double* lattice);
void S_site(double* lattice, int* action_param_n, int site_i, int site_j, double* dSij);
void S_tot(double* lattice, double* action_param_beta, int* action_param_n, double* S);
void metropolis_sweep(double* lattice, double* action_param_beta, int* action_param_n, int n_hits, double epsilon);
void microcanonical_demon_update(double* avg_accept, double num_demon_steps_inv, double* last_steps, double* lattice, double* action_param_beta, int* action_param_n, double epsilon, int file_name_index, int system_cycle);
void measure_corr(double* lattice, int measurement_num);


int main(void) {
    double action_param_beta[12] = {1.30,0.35,0.01,0.02,0.004,-0.200
                                -0.080,-0.02,-0.01,-0.005,0.02,0.01};
    int action_param_n[12] = {1,1,1,1,1,2,2,2,2,2,3,3};

    //srand(time(NULL)); // set random number seed
    srand(13);
    double epsilon = 0.1;   // how large of a perturbation for each spin during updates
    int iterations = 1000000; // how many total metrop sweeps
    int report = 1000;       // how many metrop sweeps have been completed 
    int n_hits = 5;         // how many hits on a spin during metrop before moving to next spin
    int thermal_iter = 29;  // how many metrop sweeps before record corr function and start demons
    int measure_iter = 20;  // how often to meaure correlation function
    int demon_iter = 100;     // how often to do microcanonical update between independent systems ^^ num_systems defined above

    double lattice[nx*ny*3]; // nx by ny lattice of spins with 3 degrees of freedom rotation
    

    //set up
    random_lattice(lattice); // call function to fill lattice with random spins

    int measurement_num = 0;// for filenames of correlation functions at each measurement
    int canonical_num = 0;  // for filenames of demon chains  
    int system_cycle = 0;   // keep track of how many times through the demon system 
    
                            // how many demon sweeps will be completed 
    int total_demon_sweeps = (int) (iterations - thermal_iter-1)/demon_iter;

    double avg_accept[1] = {0.0};   // record average acceptance rate within microcanonical sweeps 
    double num_demon_steps_inv = (double) 1/ (double) (total_demon_sweeps*ny*ny);
    // ^^ avg_accept += num_demon_steps_inv if accept demon update, no need to divide at the end

    // record here the last step in a chain during microcanonical sweep so can pick up system where it 
    // left off 
    double last_steps[num_terms*total_demon_sweeps];  


    // complete # iterations metropolis sweeps, microcanonical updates, and corr function measurements
    for(int iter = 0; iter < iterations; iter ++){
        if(iter%report == 0) printf("%d sweeps completed out of %d\n", iter, iterations);
        
        // complete one metropolis sweep to update lattice 
        metropolis_sweep(lattice, action_param_beta, action_param_n, n_hits, epsilon); 

        // complete a microcanonical demon sweep DOES NOT AFFECT LATTICE CHAIN
        if(iter> thermal_iter && iter%demon_iter==0){
            microcanonical_demon_update(avg_accept, num_demon_steps_inv, last_steps, lattice, action_param_beta, action_param_n, epsilon, canonical_num, system_cycle);
            canonical_num += 1; //update the file index so demon chain file can be saved for each sweep 
            // if iterated through num_demon_systems, update cycle number will work on next 
            //used for indexing last_steps (get demons from previous cycle to start each cycle)
            if(canonical_num%num_demon_systems == 0) system_cycle += 1; 
        }
        

        //record correlation function
        if(iter > thermal_iter && iter%measure_iter == 0){
            //call correlation calculator
            measure_corr(lattice, measurement_num);
            measurement_num += 1; //update the file index so each corr can be saved 
        }
    }
    
    
    printf("avg demon accepts rate = %f\n", avg_accept[0]);




    return 0;

}











void map_vec(double a, double b, double* vector){

    /*
    Map two numbers (or two 1d numpy array of same lengths), a and b, in [0, 1)
    to a vector [x, y, z] with unit norm.
    If a and b are randomly distributed in [0, 1) interval, the function will
    return randomly distributed vectors on the sphere.
    */
    double phi = 2*PI*a;
    double theta = acos(2*b-1);
    vector[0] = sin(theta)*cos(phi);
    vector[1] = sin(theta)*sin(phi);
    vector[2] = cos(theta);

    return;
}



void crossProduct(double* vect_A, double* vect_B, double* vector) {
    vector[0] = vect_A[1] * vect_B[2] - vect_A[2] * vect_B[1]; 
    vector[1] = vect_A[2] * vect_B[0] - vect_A[0] * vect_B[2]; 
    vector[2] = vect_A[0] * vect_B[1] - vect_A[1] * vect_B[0]; 
    return;
} 



double dotProduct(double* vect_A, double* vect_B, int n) { 
    double product = 0; 
    for (int i = 0; i < n; i++) 
        product = product + vect_A[i] * vect_B[i]; 
    return product; 
}


void axis_rot(double* v, double* w, double* vector){
    /*
    Given a fixed unit vector v (one of our spins) in cartesian coordinates [x,y,z],
    rotate vector w such that if w == [0, 0, 1], the function will output
    vector v. This is done by using Rodrigues' rotation formula.
   
    We will use a perturbation of w = [0,0,1] to get a perturbation of v 
    */
    // Normalize v
    double norm_v = sqrt(dotProduct(v,v,3));
    double v_norm[3] = {v[0]/norm_v, v[1]/norm_v, v[2]/norm_v};
    // Find the axis of rotation, k = [0,0,1] x v_norm
    double z[3] = {0,0,1};
    double k[3];
    crossProduct(z,v_norm, k);
    double norm_k =  sqrt(dotProduct(k,k,3));
    double k_norm[3] = {k[0]/norm_k, k[1]/norm_k, k[2]/norm_k};
    // Find angle of rotation, phi cosphi = v dot [0,0,1]
    double cosphi = dotProduct(v_norm,z,3);
    double phi = acos(cosphi);
    // Apply rotation formula
    double kxw[3]; //k cross w
    crossProduct(k_norm,w,kxw);
    for(int i=0; i<3; i++) vector[i] = w[i]*cosphi + kxw[i]*sin(phi) + k_norm[i]*dotProduct(k_norm, w,3)*(1-cosphi);
    
    return;   
}



void epsilon_update(double* v, double epsilon, double* new_v){
    /*
    Given a vector with unit norm, return a new vector, w, such that w is chosen
    randomly around the cone centered at vector v.
    epsilon is in the interval [0, 1) and the angle of the cone is given
    by arccos(2*(1-epsilon) - 1).
    */
    // First find the new random vector around a cone centered at vector [0, 0, 1]
    double a = (double)rand() / (double)RAND_MAX; 
    double b = (double)rand() / (double)RAND_MAX; 
    double c = 1.-b*epsilon;
    double dv[3];
    map_vec(a,c,dv); //from a and c get the vector dv
    //rotate this vector to be a perturbation of v 
    axis_rot(v,dv,new_v);
    return;
}


int index_lattice(int i, int j, int k){
    // row major 
    return i*ny*3 + j*3 + k;
}


void random_lattice(double* lattice){
    /*
    Return a random lattice of spins (one dimensional indexing)
    lattice[i*ny*3 + j*3 + k] gives ith row (0 to nx -1) jth column (0 to ny -1)
    kth component of spin (0,1,2)
    */
    //loop over sites 
    for(int i = 0; i < nx; i++){
        for(int j = 0; j < nx; j++){
            double a = (double)rand() / (double)RAND_MAX; 
            double b = (double)rand() / (double)RAND_MAX; 
            // get a random spin vector at this site
            double spin_vector[3];
            map_vec(a,b,spin_vector);
            // define lattice spin at this site 
            for(int k=0; k<3; k++) lattice[index_lattice(i,j,k)] = spin_vector[k]; 
        }
    }

    return;
}



void S_site(double* lattice, int* action_param_n, int site_i, int site_j, double* dSij){
    /*
    Calculate the lattice action associated with site (xi, xj).
    assume periodic boundary conditions
    */
    
    // get local spin value
    double local_spin[3];
    for(int k = 0; k < 3; k++) local_spin[k] = lattice[index_lattice(site_i, site_j, k)];
    
    // Loop over interaction terms
    for(int a=0; a < num_terms; a++){
        int n = action_param_n[a];

        double neighbor1[3];
        double neighbor2[3];
        double neighbor3[3];
        double neighbor4[3];
        double neighbor5[3];
        double neighbor6[3];
        double neighbor7[3];
        double neighbor8[3];
        
        if(a == 0 || a == 5 || a == 10){ // v= (1,0)
            for(int k = 0; k < 3; k++){
                neighbor1[k] = lattice[index_lattice((site_i + 1 + nx)%nx, site_j, k)];
                neighbor2[k] = lattice[index_lattice((site_i - 1 + nx)%nx, site_j, k)];
                neighbor3[k] = lattice[index_lattice(site_i, (site_j + 1 + ny)%ny, k)];
                neighbor4[k] = lattice[index_lattice(site_i, (site_j - 1 + ny)%ny, k)];
            }
            dSij[a] =     pow(dotProduct(local_spin, neighbor1, 3), n) 
                        + pow(dotProduct(local_spin, neighbor2, 3), n)
                        + pow(dotProduct(local_spin, neighbor3, 3), n)
                        + pow(dotProduct(local_spin, neighbor4, 3), n);
        }
            
        if(a == 1 || a == 6 || a == 11){ // v= (1,1)
            for(int k = 0; k < 3; k++){
                neighbor1[k] = lattice[index_lattice((site_i + 1 + nx)%nx, (site_j + 1 + ny)%ny, k)];
                neighbor2[k] = lattice[index_lattice((site_i - 1 + nx)%nx, (site_j + 1 + ny)%ny, k)];
                neighbor3[k] = lattice[index_lattice((site_i + 1 + nx)%nx, (site_j - 1 + ny)%ny, k)];
                neighbor4[k] = lattice[index_lattice((site_i - 1 + nx)%nx, (site_j - 1 + ny)%ny, k)];
            }
            dSij[a] =     pow(dotProduct(local_spin, neighbor1, 3), n)
                        + pow(dotProduct(local_spin, neighbor2, 3), n)
                        + pow(dotProduct(local_spin, neighbor3, 3), n)
                        + pow(dotProduct(local_spin, neighbor4, 3), n);
        }
    
        if(a == 2 || a == 7){ // v= (2,0)
            for(int k = 0; k < 3; k++){
                neighbor1[k] = lattice[index_lattice((site_i + 2 + nx)%nx, site_j, k)];
                neighbor2[k] = lattice[index_lattice((site_i - 2 + nx)%nx, site_j, k)];
                neighbor3[k] = lattice[index_lattice(site_i, (site_j + 2 + ny)%ny, k)];
                neighbor4[k] = lattice[index_lattice(site_i, (site_j - 2 + ny)%ny, k)];
            }
            dSij[a] =     pow(dotProduct(local_spin, neighbor1, 3), n)
                        + pow(dotProduct(local_spin, neighbor2, 3), n)
                        + pow(dotProduct(local_spin, neighbor3, 3), n)
                        + pow(dotProduct(local_spin, neighbor4, 3), n);
        
        }
    
        if(a == 3 || a == 8){ // v= (2,1)
            for(int k = 0; k < 3; k++){
                neighbor1[k] = lattice[index_lattice((site_i + 2 + nx)%nx, (site_j + 1 + ny)%ny, k)];
                neighbor2[k] = lattice[index_lattice((site_i + 2 + nx)%nx, (site_j - 1 + ny)%ny, k)];
                neighbor3[k] = lattice[index_lattice((site_i - 2 + nx)%nx, (site_j + 1 + ny)%ny, k)];
                neighbor4[k] = lattice[index_lattice((site_i - 2 + nx)%nx, (site_j - 1 + ny)%ny, k)];
                neighbor5[k] = lattice[index_lattice((site_i + 1 + nx)%nx, (site_j + 2 + ny)%ny, k)];
                neighbor6[k] = lattice[index_lattice((site_i + 1 + nx)%nx, (site_j - 2 + ny)%ny, k)];
                neighbor7[k] = lattice[index_lattice((site_i - 1 + nx)%nx, (site_j + 2 + ny)%ny, k)];
                neighbor8[k] = lattice[index_lattice((site_i - 1 + nx)%nx, (site_j - 2 + ny)%ny, k)];
            }
            dSij[a] =     pow(dotProduct(local_spin, neighbor1, 3), n)
                        + pow(dotProduct(local_spin, neighbor2, 3), n)
                        + pow(dotProduct(local_spin, neighbor3, 3), n)
                        + pow(dotProduct(local_spin, neighbor4, 3), n)
                        + pow(dotProduct(local_spin, neighbor5, 3), n)
                        + pow(dotProduct(local_spin, neighbor6, 3), n)
                        + pow(dotProduct(local_spin, neighbor7, 3), n)
                        + pow(dotProduct(local_spin, neighbor8, 3), n);
        }

        if(a == 4 || a == 9){ // v= (3,0)
            for(int k = 0; k < 3; k++){
                neighbor1[k] = lattice[index_lattice((site_i + 3 + nx)%nx, site_j, k)];
                neighbor2[k] = lattice[index_lattice((site_i - 3 + nx)%nx, site_j, k)];
                neighbor3[k] = lattice[index_lattice(site_i, (site_j + 3 + ny)%ny, k)];
                neighbor4[k] = lattice[index_lattice(site_i, (site_j - 3 + ny)%ny, k)];
            }
            dSij[a] =     pow(dotProduct(local_spin, neighbor1, 3), n)
                        + pow(dotProduct(local_spin, neighbor2, 3), n)
                        + pow(dotProduct(local_spin, neighbor3, 3), n)
                        + pow(dotProduct(local_spin, neighbor4, 3), n);
        }
    }

    return;
}



void S_tot(double* lattice, double* action_param_beta, int* action_param_n, double* S){
    /*
    Return the action for a given lattice and action parameters.
    Here we assume periodic boundary condition.
    */

    // loop over sites
    for(int i = 0; i < nx; i++){
        for(int j = 0; j < ny; j++){
            double dSij[num_terms];
            // get the action term values at this site
            S_site(lattice, action_param_n, i, j, dSij);
            //sum up each site with factor of 1/2 for double counting 
            for(int a = 0; a < num_terms; a++) S[a] += 0.5*dSij[a];
        }
    }
    
    return;
}



void metropolis_sweep(double* lattice, double* action_param_beta, int* action_param_n, int n_hits, double epsilon){
    /*
    Perform a sweep of metropolis update to the input lattice. Each site will be
    updated n times regardless of being accepted or rejected.
    epsilon is in [0, 1) that controls how big the proposed updates will be with
    0 being identity (no changes in updates).
    */
    for(int i = 0; i < nx; i++){
        for(int j = 0; j < ny; j++){
            //multi-hit updates
            for(int n = 0; n < n_hits; n++){
                double dSij_old[num_terms];
                double dSij_new[num_terms];
                double old_spin[3];
                double new_spin[3];
          
                // get initial spin state (in case need to flip back)
                for(int k = 0; k < 3; k++) old_spin[k] = lattice[index_lattice(i,j,k)];
                //get initial action
                S_site(lattice, action_param_n, i, j, dSij_old);

                // get new spin state
                epsilon_update(old_spin, epsilon, new_spin);
                // update lattice site 
                for(int k = 0; k < 3; k++) lattice[index_lattice(i,j,k)] = new_spin[k];
                // get new action
                S_site(lattice, action_param_n, i, j, dSij_new);

                //change in action
                // action = - sum beta_i S_i  summing over action terms
                // dS = old - new due to - sign in definition of action
                double dS =  dotProduct(action_param_beta, dSij_old, num_terms) 
                            -dotProduct(action_param_beta, dSij_new, num_terms);
                // accept reject step 
                double random =  (double)rand() / (double)RAND_MAX; 
                if(random >= exp(-dS)){ // REJECT 
                    // REVERT lattice site 
                    for(int k = 0; k < 3; k++) lattice[index_lattice(i,j,k)] = old_spin[k];
                }
    
            } // end loop over hits
        } // end j loop
    } // end i loop

    return;
}




void microcanonical_demon_update(double* avg_accept, double num_demon_steps_inv,  double* last_steps, double* lattice, double* action_param_beta, int* action_param_n, double epsilon, int file_name_index, int system_cycle){
    /*
    Do sweep of spin update and compute dS_alpha, the change in the action terms 
    for each term due to that spin update. Joint system S = sum beta_alpha (d_alpha - S_alpha)
    action remains the same -> 
    update  S_alpha -> S_alpha + dS_alpha 
            d_alpha -> d_alpha + dS_alpha
    
    save demon chain to file for each canonical sweep 
    */

    //record the entire demon chain for this sweep, then save this to file
    double demon_chain_current_sweep[num_terms*(nx*ny+1)]; // plus 1 for initial

    //buffer for "current" demon value to be updated
    double d_current[num_terms]; // add dS to get d_new[a] and see if in range
    

    // if this is the first set of demons, fill with demons set in middle of range
    if(file_name_index < num_demon_systems){
        for(int a = 0; a < num_terms; a++){
            demon_chain_current_sweep[a] = 0.5*(d_max - d_min) + d_min;
            d_current[a] = demon_chain_current_sweep[a];
        }
    }

    

    //otherwise, take the initial step in this chain to be the last step in the 
    //previous chain of the same system
    else{
        int index_system_last_previous = (file_name_index - num_demon_systems)*num_terms;
        for(int a = 0; a < num_terms; a++){
            demon_chain_current_sweep[a] = last_steps[index_system_last_previous  + a]; 
            d_current[a] = demon_chain_current_sweep[a];
        }
    }


    // buffer for next in chain 
    double d_new[num_terms]; // fill at each ij site

    // place holder for single term to compare to d_min, d_max
    double d_new_a; // d_current[a] + dS[a] may or may not fit in backpack 
    
    bool fit_in_backpack; // true or false determines whether to accept a demon update
    
    // save a copy of the lattice to modify here
    double* demon_lattice = lattice;
    
    double dSij_old[num_terms]; //contains action terms before a flip
    double dSij_new[num_terms]; //contains action terms after a flip
    double old_spin[3]; //holds value of local spin before
    double new_spin[3]; //holds value of local spin after 

    double dS[num_terms]; //dSij_new - dSij_old
    
    //sweep over lattice to get nx*ny more demon values 
    for(int i = 0; i < nx; i++){
        for(int j = 0; j < ny; j++){
        
            // get initial spin state (in case need to flip back)
            for(int k = 0; k < 3; k++) old_spin[k] = demon_lattice[index_lattice(i,j,k)];
            //get initial action
            S_site(demon_lattice, action_param_n, i, j, dSij_old);

            // get new spin state
            epsilon_update(old_spin, epsilon, new_spin);
            
            // update lattice site 
            for(int k = 0; k < 3; k++) demon_lattice[index_lattice(i,j,k)] = new_spin[k];
            // get new action
            S_site(demon_lattice, action_param_n, i, j, dSij_new);

            // change in action term 
            fit_in_backpack = true; //resets this to true for each ij site 
            for(int a = 0; a < num_terms; a++){
                dS[a] = dSij_new[a] - dSij_old[a];
                
                //calculat new demon value
                d_new_a = d_current[a] + dS[a]; 
                // decide if they all fit in backpack
                if(d_new_a < (double)d_min){
                    fit_in_backpack = false;
                }
                if(d_new_a > (double)d_max){
                    fit_in_backpack = false;
                }
            }
             
            // if fit, update d_new and spin (already updated)
            if(fit_in_backpack == true){
                avg_accept[0] += num_demon_steps_inv;
                for(int a = 0; a < num_terms; a++){
                    d_new[a] = d_current[a] + dS[a];
                }
                
            }
            //otherwise d_new = d_old and revert spin
            else{
                for(int a = 0; a < num_terms; a++){
                    d_new[a] = d_current[a];        
                }
                for(int k = 0; k < 3; k++) demon_lattice[index_lattice(i,j,k)] = old_spin[k];
            }
            
            // put new demon at the end of the chain 
            int current_index = (i*ny + j + 1)*num_terms; //plus 1 for initial
            for(int a = 0; a < num_terms; a++) demon_chain_current_sweep[current_index + a] = d_new[a]; 

            // "new" demon becomes current demon for next ij site
            for(int a = 0; a < num_terms; a++) d_current[a] = d_new[a];
            
            
        } // end j loop
    } // end i loop
            
   
    
    
    //record demons in this sweep
    FILE* fp_demon;
    char file_name_demon[64];
    sprintf(file_name_demon, "demons_systems_N1M/demon_file_%d.txt", file_name_index);
    fp_demon = fopen(file_name_demon, "w+");

    //save chain to file
    for(int i = 0; i < nx*ny + 1; i++){
        for(int a = 0; a < num_terms; a++){
            fprintf(fp_demon, "%0.5f ", demon_chain_current_sweep[i*num_terms + a]);
        }
        fprintf(fp_demon, "\n");
    } 
    fclose(fp_demon);
    

    //save last step in this chain to last_steps to be used as initial 
    //step in next sweep for this system (index is nx*ny since there are nx*ny+1 total)
    for(int a = 0; a < num_terms; a++) last_steps[file_name_index*num_terms +a] = demon_chain_current_sweep[nx*ny*num_terms + a];
        
    return;
}





void measure_corr(double* lattice, int measurement_num){
    /*
    Measure two point correlation function of given range of distance separation.
    Assume square lattice.
    */
    //int num_at_radius[radial_extent];
    //for(int d = 0; d < radial_extent; d++) num_at_radius[d] = 0;
    //int radii[radial_extent];

    // record observations at each distance here 
    double observations[nx];
    for(int d = 0; d < nx; d++) observations[d] = 0.0;

    for(int i = 0; i < nx; i++){
        for(int j = 0; j < ny; j++){
            //get local spin
            double local_site[3];
            double remote_site[3];
            for(int k = 0; k < 3; k++) local_site[k] = lattice[index_lattice(i,j,k)];
            
            //loop over horizontal displacement
            for(int dx = 0; dx < nx; dx++){
                //get spin at this remote position
                for(int k = 0; k < 3; k++) remote_site[k] = lattice[index_lattice((i+dx)%nx,j,k)];
                // record observation of correlation 
                // each point contributed 4 measurements at displacement d
                observations[dx] += dotProduct(local_site, remote_site, 3)/((double)(4*nx*ny));                
            } //end dx loop

            //loop over vertical displacement
            for(int dy = 0; dy < ny; dy++){
                //get spin at this remote position
                for(int k = 0; k < 3; k++) remote_site[k] = lattice[index_lattice(i,(j+dy)%ny,k)];
                // record observation of correlation 
                // each point contributed 4 measurements at displacement d
                observations[dy] += dotProduct(local_site, remote_site, 3)/((double)(4*nx*ny));                
            } //end dy loop 
        
        }// end j loop
    } //end i loop
    
    FILE *fp;
    char filename[64];
    sprintf(filename, "correlation_data_N1M/correlations_c_it%d.txt", measurement_num);
    fp = fopen(filename, "w+");

    //assumes square
    for(int d = 0; d < nx; d++){
        observations[d] = observations[d];        
        fprintf(fp,"%0.3f\n",observations[d]); 
    }
    fclose(fp);
    return;
}











